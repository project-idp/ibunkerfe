import react, { useState } from 'react';
import CardsList from '../components/CardsList';
import Modal from '../components/Modal';

function Home() {
    const [modalOn, setModalOn] = useState(false);
    const [choice, setChoice] = useState(false);

    const handleButton = () => {
        setModalOn(true);
    }

    return (
        <div className='fixed top-[15%] inset-x-[125px] w-full h-full'>
            <div className='relative top-[5%] inset-x-[80%]'>
                <button className='bg-custom_light_blue hover:bg-green-400 rounded-md' onClick={handleButton}>
                    <div className='p-2'>
                        Add bunker
                    </div>
                </button>
            </div>
            <div className='relative top-[20%]'>
                <CardsList />
            </div>
            <div>
            {modalOn && <Modal setModalOn={setModalOn} setChoice={setChoice}/>}
            </div>
        </div>
    )
}

export default Home;